package bank.Classes;

public class Person {

	public String name;
	public String lastname;
	public String city;
	public String Country;
	public String password;
	public String email;
	public String birthday;
	public String datetime;

	public Person() {

	}

	public Person(String name, String lastname, String city, String Country,
			String email, String birthday, String password, String datetime) {
		this.name = name;
		this.lastname = lastname;

		this.city = city;
		this.Country = Country;
		this.email = email;
		this.birthday = birthday;
		this.password = password;
		this.datetime = datetime;

	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", lastname=" + lastname + ", city="
				+ city + ", Country=" + Country + ", password=" + password
				+ ", email=" + email + ", birthday=" + birthday + ", datetime="
				+ datetime + "]";
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

}
