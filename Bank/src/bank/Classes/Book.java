package bank.Classes;

public class Book {
	private int id;
	private String name;
	private String date_checkin;
	private String date_checkout;
	private String state;
	private String host_name;

	public Book() {

	}

	public Book(int id, String name, String date_checkin, String date_checkout,
			String state, String host_name) {
		super();
		this.id = id;
		this.name = name;
		this.date_checkin = date_checkin;
		this.date_checkout = date_checkout;
		this.state = state;
		this.host_name = host_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate_checkin() {
		return date_checkin;
	}

	public void setDate_checkin(String date_checkin) {
		this.date_checkin = date_checkin;
	}

	public String getDate_checkout() {
		return date_checkout;
	}

	public void setDate_checkout(String date_checkout) {
		this.date_checkout = date_checkout;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getHost_name() {
		return host_name;
	}

	public void setHost_name(String host_name) {
		this.host_name = host_name;
	}

}
