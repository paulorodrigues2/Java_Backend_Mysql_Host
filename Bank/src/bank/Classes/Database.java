package bank.Classes;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Database {

	static Connection MysqlConn;
	static PreparedStatement MysqlPrepareStat;

	public Database() {
		try {
			// DriverManager: The basic service for managing a set of JDBC
			// drivers.
			MysqlConn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/hosting", "root", "");
			if (MysqlConn != null) {
				System.out
						.println("Connection Successful! Enjoy. Now it's time to push data");
			} else {
				System.out.println("Failed to make connection!");
			}
		} catch (SQLException e) {
			System.out.println("MySQL Connection Failed!");
			e.printStackTrace();
			return;
		}
	}

	/*
	 * public void UserBuyProduct(List<Product> products) {
	 * 
	 * }
	 */

	public void ListHouses(List<Casas> houses) {
		try {
			String insertQueryStatement = "Select * from house";

			MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);

			ResultSet result = MysqlPrepareStat.executeQuery();
			while (result.next()) {

				Casas Product = new Casas();
				Product.setId(result.getInt("id"));
				Product.setName(result.getString("name"));
				Product.setLocalization(result.getString("localization"));
				Product.setPrice(result.getDouble("price"));
				houses.add(Product);

			}

		} catch (

		SQLException e) {
			e.printStackTrace();
		}

	}

	/*
	 * public void InsertPurchase(int UserId, int indexname, List<Person>
	 * persons, List<Product> products) { String date = new
	 * SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
	 * .format(Calendar.getInstance().getTime());
	 * 
	 * try { String insertQueryStatement =
	 * "INSERT INTO purchase ( user_id, product_id, datetime) VALUES (?,?,?)";
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * MysqlPrepareStat.setInt(1, UserId); MysqlPrepareStat.setInt(2,
	 * indexname); MysqlPrepareStat.setString(3, date);
	 * 
	 * // execute insert SQL statement MysqlPrepareStat.executeUpdate();
	 * 
	 * System.out.println("Inserted!");
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); } }
	 */

	public void InsertRating(List<Casas> casasList, int indexvalue) {
		try {
			String updateQueryStatement = "Update house SET house.likes=(house.likes+1) where id=?";

			MysqlPrepareStat = MysqlConn.prepareStatement(updateQueryStatement);
			MysqlPrepareStat.setInt(1, indexvalue);

			// execute insert SQL statement
			MysqlPrepareStat.executeUpdate();
			System.out.println("Rating da casa foi efectuado com sucesso!");

		} catch (

		SQLException e) {
			e.printStackTrace();
		}

	}

	public void UpdateProduct(List<Casas> products, int indexvalue) {

		try {
			String updateQueryStatement = "Update product SET product.likes=(likes.quantity+1) where id=?";

			MysqlPrepareStat = MysqlConn.prepareStatement(updateQueryStatement);
			MysqlPrepareStat.setInt(1, indexvalue);

			// execute insert SQL statement
			MysqlPrepareStat.executeUpdate();

		} catch (

		SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * public void GetAllPurchase(List<Product> products, List<Person> persons,
	 * int indexvalue) {
	 * 
	 * try {
	 * 
	 * String updateQueryStatement =
	 * "Update product SET product.quantity=(product.quantity-1) where id=?";
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(updateQueryStatement);
	 * MysqlPrepareStat.setInt(1, indexvalue);
	 * 
	 * // execute insert SQL statement MysqlPrepareStat.executeUpdate();
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); } }
	 * 
	 * public void ListProductPrice(List<Product> products) { try { String
	 * insertQueryStatement = "Select * from product order by price DESC";
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * 
	 * ResultSet result = MysqlPrepareStat.executeQuery(); while (result.next())
	 * {
	 * 
	 * Product Product = new Product(); Product.setId(result.getInt("id"));
	 * Product.setName(result.getString("name"));
	 * Product.setCode(result.getString("code"));
	 * Product.setCode(result.getString("description"));
	 * Product.setPrice(result.getDouble("price"));
	 * Product.setQuantity(result.getInt("quantity")); products.add(Product);
	 * 
	 * }
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); }
	 * 
	 * }
	 * 
	 * public void AddProductToDB(List<Product> products) {
	 * 
	 * try { String insertQueryStatement =
	 * "INSERT INTO product ( name, code, price, quantity, description) VALUES (?,?,?,?,?)"
	 * ;
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * MysqlPrepareStat.setString(1, products.get(products.size() - 1)
	 * .getName()); MysqlPrepareStat.setString(2, products.get(products.size() -
	 * 1) .getCode()); MysqlPrepareStat.setDouble(3,
	 * products.get(products.size() - 1) .getPrice());
	 * MysqlPrepareStat.setInt(4, products.get(products.size() - 1)
	 * .getQuantity()); MysqlPrepareStat.setString(5,
	 * products.get(products.size() - 1) .getDescription());
	 * 
	 * // execute insert SQL statement MysqlPrepareStat.executeUpdate();
	 * System.out.println("Inserted!");
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); }
	 * 
	 * }
	 * 
	 * public void addUserDataToDB(List<Person> persons) {
	 * 
	 * try { String insertQueryStatement =
	 * "INSERT INTO user ( name, sobrename, country, city, email, password, datetime, birthday) VALUES (?,?,?,?,?,?,?,?)"
	 * ;
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * MysqlPrepareStat.setString(1, persons.get(persons.size() - 1)
	 * .getName()); MysqlPrepareStat.setString(2, persons.get(persons.size() -
	 * 1) .getLastname()); MysqlPrepareStat.setString(4,
	 * persons.get(persons.size() - 1) .getCountry());
	 * MysqlPrepareStat.setString(5, persons.get(persons.size() - 1)
	 * .getCity()); MysqlPrepareStat.setString(6, persons.get(persons.size() -
	 * 1) .getEmail()); MysqlPrepareStat.setString(7, persons.get(persons.size()
	 * - 1) .getPassword()); MysqlPrepareStat.setString(8,
	 * persons.get(persons.size() - 1) .getDatetime());
	 * MysqlPrepareStat.setString(9, persons.get(persons.size() - 1)
	 * .getBirthday());
	 * 
	 * // execute insert SQL statement MysqlPrepareStat.executeUpdate();
	 * System.out.println("Inserted!");
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); } }
	 * 
	 * public void Login(List<Person> persons) {
	 * 
	 * }
	 * 
	 * public int GetIdUser(List<Person> persons, String email) {
	 * 
	 * int count = 0; try { String insertQueryStatement =
	 * "Select * from user where email=?";
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * MysqlPrepareStat.setString(1, email);
	 * 
	 * ResultSet result = MysqlPrepareStat.executeQuery(); while (result.next())
	 * {
	 * 
	 * count = result.getInt("id");
	 * 
	 * }
	 * 
	 * return count;
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); } return count;
	 * 
	 * }
	 * 
	 * public boolean CheckIfExistUser(List<Person> persons) {
	 * 
	 * try { String insertQueryStatement =
	 * "Select count(*) as number from user where email=? and password=?";
	 * 
	 * MysqlPrepareStat = MysqlConn.prepareStatement(insertQueryStatement);
	 * MysqlPrepareStat.setString(1, persons.get(persons.size() - 1)
	 * .getEmail()); MysqlPrepareStat.setString(2, persons.get(persons.size() -
	 * 1) .getPassword());
	 * 
	 * ResultSet result = MysqlPrepareStat.executeQuery(); while (result.next())
	 * {
	 * 
	 * int count = result.getInt("number");
	 * 
	 * if (count > 0) {
	 * 
	 * return true;
	 * 
	 * } else { return false; } }
	 * 
	 * } catch (
	 * 
	 * SQLException e) { e.printStackTrace(); } return false; }
	 * 
	 * }
	 */

}
