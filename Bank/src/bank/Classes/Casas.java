package bank.Classes;

public class Casas {
	private int id;
	private String name;
	private String localization;
	private int quantidade;
	private String estado;
	private double price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocalization() {
		return localization;
	}

	public void setLocalization(String localization) {
		this.localization = localization;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Casas(int id, String name, String localization, int quantidade,
			String estado, double price) {
		super();
		this.id = id;
		this.name = name;
		this.localization = localization;
		this.quantidade = quantidade;
		this.estado = estado;
		this.price = price;
	}

	public Casas() {
		super();
	}

}
